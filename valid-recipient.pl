#!/usr/bin/perl -w

use strict;

use FindBin;
use Mail::Verp;
use DBI;

use vars qw(
	    $dsn $dbh

	    $dbtype $dbuser $dbauth $dbhost $dbase
	    $mail_domain

	    $query_address_stmt $query_address_handle

	    $num_list
	   );

# ----------------------------------------------------------------------

# Default values
$mail_domain = "localhost";

Mail::Verp->separator('+');

$dbtype = "mysql";
$dbase  = "";
$dbhost = "localhost";
$dbuser = "";
$dbauth = "";

do "$FindBin::Bin/mail.conf";

# ----------------------------------------------------------------------

$query_address_stmt=<<EOF;
SELECT 1 FROM og2list_groups
WHERE recipient
LIKE ?
EOF

$dsn = "DBI:$dbtype:$dbase:$dbhost";
$dbh = DBI->connect($dsn, $dbuser, $dbauth) or die;

$query_address_handle = $dbh->prepare($query_address_stmt) or die;

unless (exists $ARGV[0]) {
  die;
}

$query_address_handle->execute($ARGV[0]) or die;

$num_list=0;
while ($query_address_handle->fetchrow_array()) {
  $num_list++;
}

if ($query_address_handle->rows() >= 1) {
  print "$ARGV[0] is a valid address.\n";
  exit 0;
} else {
  print "$ARGV[0] is NOT a valid address ($num_list).\n";
  exit 1;
}

# Local Variables:
# mode:cperl
# End:
