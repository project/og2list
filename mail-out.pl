#!/usr/bin/perl -w


# THIS IS PROTOTYPE CODE AND SHOULD NOT BE USED IN PRODUCTION SYSTEMS!

# To quickly read the Perldoc below on a Unix system, use
#
# $ pod2man <FILE> | man -l -

=pod

=head1 NAME

mail-out.pl - outgoing mail handler for og2list.module

=head1 Synopsis

C<mail-out.pl [-d]>

=head1 Description

mail-out.pl reads messages fed into the og2list outgoing queue and
sends them out via a locelly installed MTA's /usr/bin/sendmail
interface.

If the -d ("digest") switch is set, send multipart/digest messages to
those users that have digest delivery configured.

=head1 Unfinished work, TODO's

=item ...

=head1 License

mail-out.pl, a helper script to send mail queued from the og2list
Drupal module.

Copyright (C) 2006 Hilko Bengen

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

=cut

use strict;

use FindBin;
use Mail::Verp; Mail::Verp->separator('+');
use Net::SMTP;
use MIME::Entity;
use Getopt::Std;
# Used for encoding the subject (if necessary)
use Encode qw(encode);
use DBI;

use vars qw(
   $dsn $dbh

   $get_content_stmt $get_rcpt_stmt $set_sentflag_stmt $get_sender_stmt
   $get_arg_stmt $get_create_mail_stmt $get_del_arg_stmt

   $get_digest_rcpt_stmt $clear_digest_rcpt_stmt

   $get_content_handle $get_rcpt_handle $set_sentflag_handle $get_sender_handle
   $get_arg_handle $get_create_mail_handle $get_del_arg_handle

   $get_digest_rcpt_handle $clear_digest_rcpt_handle

   $dbtype $dbuser $dbauth $dbhost $dbase $mail_domain

   $set_reply_to

   $send_method $batch_size

   $sendmail_binary @sendmail_args $bsmtp_binary @bsmtp_args $smtp_server

   %opts
   $do_digest

   $mid $nid $from_name $from_address $to_name $to_address
   $subject $message_id $content_type $body
   %digest_msgs
);

use constant {
  OG2LIST_OUTGOING_NOT_SENT => 1,
  OG2LIST_OUTGOING_RECEIVED_BY_MTA => 2,
  OG2LIST_OUTGOING_LOCAL_ERROR => 3,
  OG2LIST_OUTGOING_REMOTE_ERROR => 4,
  OG2LIST_OUTGOING_POSTPONED => 5,
  OG2LIST_OUTGOING_MODERATED => 6,
  OG2LIST_OUTGOING_DIGEST => 7,
};

# ------------------------------------------------------------------------

# Default values
$mail_domain = "localhost.localdomain";

$smtp_server = "localhost:25";
$sendmail_binary = "/usr/lib/sendmail";
@sendmail_args = ("-oi","-oem","-f");
$bsmtp_binary = "/usr/sbin/rsmtp";
@bsmtp_args = ();

$dbtype = "mysql";
$dbase  = "";
$dbhost = "localhost";
$dbuser = "";
$dbauth = "";

$set_reply_to = 0;

do "$FindBin::Bin/mail.conf";

# ------------------------------------------------------------------------

$get_content_stmt =<<EOF;
SELECT DISTINCT c.mid,
                c.from_name,
                c.from_address,
                c.to_name,
                c.to_address,
                c.subject,
                c.msgid,
                c.content_type,
                c.body,
                r.nid
FROM og2list_outgoing_recipients AS r,
     og2list_outgoing_content AS c
WHERE (r.mid = c.mid) AND (r.status = ?)
EOF

$get_arg_stmt =<<EOF;
SELECT mid,
       nid,
       timestamp,
       status,
       uid
FROM og2list_outgoing_arguments WHERE status NOT IN (?, ?)
EOF

$get_del_arg_stmt =<<EOF;
DELETE FROM og2list_outgoing_arguments WHERE mid = ?
EOF

$get_create_mail_stmt =<<EOF;
INSERT INTO og2list_outgoing_recipients 
        (mid, nid, recipient_name, recipient_address, timestamp, status) 
        SELECT ?, ?, u.name AS recipient_name, u.mail AS recipient_address, ?, IF(ou.mail_type = 7, 7, 1) 
FROM og_uid ou INNER JOIN users u ON ou.uid = u.uid WHERE ((ou.mail_type = 1 AND u.uid != ?) OR ou.mail_type = 7) AND ou.nid = ? AND u.status > 0 AND ou.is_active >= 1
EOF

$get_rcpt_stmt=<<EOF;
SELECT recipient_address
FROM og2list_outgoing_recipients
WHERE (mid = ?) AND (status = ?)
EOF

$get_digest_rcpt_stmt=<<EOF;
SELECT DISTINCT recipient_address
FROM og2list_outgoing_recipients
WHERE (nid = ?) AND (status = ?)
EOF

$get_sender_stmt=<<EOF;
SELECT DISTINCT g.recipient
FROM og2list_outgoing_recipients AS r, og2list_groups AS g
WHERE (r.nid = g.nid) AND (r.mid = ?)
EOF

# FIXME: Misleading name
$set_sentflag_stmt=<<EOF;
DELETE FROM og2list_outgoing_recipients
WHERE (mid = ?) AND (recipient_address = ?)
EOF

$clear_digest_rcpt_stmt=<<EOF;
DELETE FROM og2list_outgoing_recipients
WHERE (nid = ?) AND (status = ?)
EOF

# ----------------------------------------------------------------------

sub send_bsmtp($$$$) {
  my $mids = shift;
  my $content = shift;
  my $sender = shift;
  my $recipients = shift;
  
  open(BSMTP, , '|-') || exec($bsmtp_binary, @bsmtp_args);
  foreach my $rcpt_addr (@$recipients) {
    my $verp_addr = Mail::Verp->encode($sender, $rcpt_addr);
    print BSMTP "MAIL FROM:<$verp_addr>\n";
    print BSMTP "RCPT TO:<$rcpt_addr>\n";
    print BSMTP "DATA\n";
    $content =~ s/^\./\.\./mg;
    print BSMTP $content;
    print BSMTP "\n.\n";
  }
  close(BSMTP) && map {$set_sentflag_handle->execute($mid, $_)} @$recipients;
}

sub send_sendmail($$$$) {
  my $mids = shift;
  my $content = shift;
  my $sender = shift;
  my $recipients = shift;

  foreach my $rcpt_addr (@$recipients) {
    my $verp_addr = Mail::Verp->encode($sender, $rcpt_addr);
    open(SENDMAIL, '|-') || exec($sendmail_binary, @sendmail_args,
			       $verp_addr, $rcpt_addr);
    print SENDMAIL $content;
    close(SENDMAIL) && $set_sentflag_handle->execute($mid, $rcpt_addr);
  }
}

sub send_smtp($$$$) {
  my $mids = shift;
  my $content = shift;
  my $sender = shift;
  my $recipients = shift;

  my $smtp = Net::SMTP->new($smtp_server);
  unless ($smtp) {
    print STDERR "ERROR: Could not connect to $smtp_server.\n";
    return;
  }

  foreach my $rcpt_addr (@$recipients) {
    my $verp_addr = Mail::Verp->encode($sender, $rcpt_addr);
    $smtp->mail($verp_addr) or do {$smtp->reset(); next};
    $smtp->to($rcpt_addr) or do {$smtp->reset(); next};
    $smtp->data($content) or do {$smtp->reset(); next};

    $set_sentflag_handle->execute($mid, $rcpt_addr);
  }
  $smtp->quit();
}

sub send_blackhole($$$$) {
  my $mids = shift;
  my $content = shift;
  my $sender = shift;
  my $recipients = shift;

  map {$set_sentflag_handle->execute($mid, $_)} @$recipients;
  return;
}

# ----------------------------------------------------------------------

getopts('d', \%opts);
$do_digest = $opts{'d'};

# Make database connection
$dsn = "DBI:$dbtype:$dbase:$dbhost";
$dbh = DBI->connect($dsn, $dbuser, $dbauth) or die;

# Work around charset encoding confusion.
if ($dbtype eq 'mysql') { $dbh->do('SET NAMES utf8'); }

# Set up queries
$get_content_handle = $dbh->prepare($get_content_stmt) or die;
$get_rcpt_handle = $dbh->prepare($get_rcpt_stmt) or die;
$get_digest_rcpt_handle = $dbh->prepare($get_digest_rcpt_stmt) or die;
$set_sentflag_handle = $dbh->prepare($set_sentflag_stmt) or die;
$clear_digest_rcpt_handle = $dbh->prepare($clear_digest_rcpt_stmt) or die;
$get_sender_handle = $dbh->prepare($get_sender_stmt) or die;
$get_arg_handle = $dbh->prepare($get_arg_stmt) or die;
$get_del_arg_handle = $dbh->prepare($get_del_arg_stmt) or die;
$get_create_mail_handle = $dbh->prepare($get_create_mail_stmt) or die;


if ($do_digest) {
  $get_content_handle->execute(OG2LIST_OUTGOING_DIGEST);
  while ( ($mid, $from_name, $from_address, $to_name, $to_address,
	   $subject, $message_id, $content_type, $body, $nid) =
	  $get_content_handle->fetchrow_array() ) {
    # Build an entry ($nid) in %digest_msgs for every ogroup and add
    # the digest message
    unless (exists $digest_msgs{$nid}) {
      $get_sender_handle->execute($mid);
      (my $localpart) = $get_sender_handle->fetchrow_array();
      my $sender = "$localpart\@$mail_domain";
      ${$digest_msgs{$nid}}{sender} = $sender;

      ${$digest_msgs{$nid}}{content} = MIME::Entity->build
	("Subject"  => "Digest for $sender",
	 "From"     => "$sender",
	 "To"       => "$sender",
	 "Type"     => "multipart/digest",
         "Encoding" => "-SUGGEST",
         "Charset"  => "utf-8",
	 "X-Mailer" => undef);

      # Get the list of addresses that are currently waiting a message
      # for this digest
      #
      $get_digest_rcpt_handle->execute($nid, OG2LIST_OUTGOING_DIGEST);

       my $recipient_address;
       while ( ($recipient_address =
 		      $get_digest_rcpt_handle->fetchrow_array()) ) {
           # print "- $recipient_address \n";
           push @{${$digest_msgs{$nid}}{recipients}}, $recipient_address;
       }
    }

    # Add each message to the multipart/digest container.
    my $msg = MIME::Entity->build
      ("From"       => "$from_name <$from_address>",
       "To"         => "$to_name <$to_address\@$mail_domain>",
       "Subject"    => encode('MIME-Header', "$subject"),
       "Type"       => "$content_type",
       "Encoding"   => "-SUGGEST",
       "Charset"    => "utf-8",
       "Message-ID" => "$message_id",
       "Data"       => "$body",
       "X-Mailer"   => undef);
    if ($set_reply_to != 0) {
      $msg->head()->add('Reply-To', "<$to_address\@$mail_domain>");
    }
    my $container = MIME::Entity->build
      ("Type" => "message/rfc822",
       "Data" => $msg->stringify(),
       "Subject"    => encode('MIME-Header', "$subject"),
       "Description" => encode('MIME-Header', "$subject"),
       "X-Mailer"   => undef);
    ${$digest_msgs{$nid}}{content}->add_part($container);
    push @{${$digest_msgs{$nid}}{mids}}, $mid;
  }

  # Actually send the message
  foreach (keys %digest_msgs) {
    my %msg = %{$digest_msgs{$_}};
    # print "Sending Message $mid to\n";
    my $content = $msg{content}->stringify();
    my $sender = $msg{sender};
    my @mids = @{$msg{mids}};
    # Get a bunch of recipients
    while (my @recipients =
	    splice @{$digest_msgs{$_}{recipients}},0,$batch_size) {
      # print "- ".join(', ', @recipients)."\n";
      foreach ($send_method) {
	/^sendmail$/i && do { send_sendmail([$mid], $content, $sender,
					    [@recipients]); last; };
	/^bsmtp$/i && do { send_bsmtp([$mid], $content, $sender,
				      [@recipients]); last; };
	/^smtp$/i && do { send_smtp([$mid], $content, $sender,
				    [@recipients]); last; };
	/^blackhole$/i && do { send_blackhole([$mid], $content, $sender,
					      [@recipients]); last; };
	die("Value of \$send_method not recognized: $send_method");
      }
    }

    # mids for this nid (key) have been sent, clear it from
    # the outgoing_recipients table
    ##
    $clear_digest_rcpt_handle->execute($_, OG2LIST_OUTGOING_DIGEST);
  }
} else {
  $get_content_handle->execute(1);
  while ( ($mid, $from_name, $from_address, $to_name, $to_address,
	   $subject, $message_id, $content_type, $body, $nid) =
	  $get_content_handle->fetchrow_array() ) {
    my $msg = MIME::Entity->build
      ("From"       => "$from_name <$from_address>",
       "To"         => "$to_name <$to_address\@$mail_domain>",
       "Subject"    => encode('MIME-Header', "$subject"),
       "Type"       => "$content_type",
       "Encoding"   => "-SUGGEST",
       "Charset"    => "utf-8",
       "Message-ID" => "$message_id",
       "Data"       => "$body",
       "X-Mailer"   => undef);
    if ($set_reply_to != 0) {
      $msg->head()->add('Reply-To', "<$to_address\@$mail_domain>");
    }
    
    # print "Sending Message $mid to\n";
    $get_sender_handle->execute($mid);
    (my $localpart) = $get_sender_handle->fetchrow_array();
    my $sender = "$localpart\@$mail_domain";
    my $content = $msg->stringify();
    $get_rcpt_handle->execute($mid,1);
    
    my $r;
    # Get a bunch of recipients through $get_rcpt_handle
    while (($r = $get_rcpt_handle->fetchall_arrayref( undef, $batch_size))
	   and ($#$r >=0 ) ) {
      my @recipients = map { $$_[0] } @$r;
      # print "- ".join(', ', @recipients)."\n";
      foreach ($send_method) {
	/^sendmail$/i && do { send_sendmail([$mid], $content, $sender,
					    [@recipients]); last; };
	/^bsmtp$/i && do { send_bsmtp([$mid], $content, $sender,
				      [@recipients]); last; };
	/^smtp$/i && do { send_smtp([$mid], $content, $sender,
				    [@recipients]); last; };
	/^blackhole$/i && do { send_blackhole([$mid], $content, $sender,
					      [@recipients]); last; };
	die("Value of \$send_method not recognized: $send_method");
      }
    }
  }
}

# Generate new mails
$get_arg_handle->execute(5,6);

while ( (my $mid, my $nid, my $timestamp, my $status, my $uid) = 
	$get_arg_handle->fetchrow_array()) {
  $get_create_mail_handle->execute($mid, $nid, $timestamp, $uid, $nid);
  $get_del_arg_handle->execute($mid);
}


# Local Variables:
# mode:cperl
# End:
