#!/usr/bin/perl -w


# THIS IS PROTOTYPE CODE AND SHOULD NOT BE USED IN PRODUCTION SYSTEMS!

# To quickly read the Perldoc below on a Unix system, use
#
# $ pod2man mail-in.pl | man -l -

=pod

=head1 NAME

mail-in.pl - incoming mail handler for og2list.module

=head1 Synopsis

C<mail-in.pl [-d] -f sender -t recipient>

=head1 Description

mail-in.pl expects a mail message conforming to RfC 2822 and possibly
containing one or more MIME parts conforming to RfC 2046ff. to be fed
to its standard input. In the default mode of operation, it is
intended to be run from a mail transfer agent.

Both sender and recipient parameters are mandatory.

If the -d ("debug") switch is set, the processed message body, along
with some pseudo headers, is printed to standard output, instead of
being queued. It is UTF-8 encoded, so non-ASCII will only show
correctly on terminals that have their character encoding set to
UTF-8.

=head1 TODO's, incorrectly/incompletely implemented issues

=over

=item Determining sender and recipient(s) from headers that might
have been inserted by MTA

=back

=head1 Integration into MTA setup (Exim 4.x)

=over

=item Global

The global section of Exim's configuration file is the place to define
variables and installation-specific parameters such as the SQL
location below.

See Section 9.21 of the Exim specification for details.

The user/group ID and the home directory need not match any other user
account on the system, in particular not the webserver's or the web
application's user. mail-in.pl only needs to access its configuration
file and the og2list_* tables in the SQL database for which the
credentials are stored in the configuration file.

  OG2LIST_HOME = /home/og2list
  OG2LIST_UID  = og2list
  OG2LIST_GID  = og2list

  hide mysql_servers = localhost/drupal/dbuser/dbpass

=item Router

Routers are rules that determine how a destination address should be
handled. The following piece of code assigns addresses to a transport
rule called og2list_transport which is defined below.

Note that the since Routers are evaluated one after anotehr, their
order in the configuration file will most likely affect overall
behavior.

See section 3.8 of the Exim specification for details.

  og2list:
    debug_print "R: og2list from $return_path \
      for $local_part@$domain"
    driver = accept
    transport = og2list_transport
    condition = ${lookup mysql{SELECT 1 FROM og2list_groups \
                               WHERE recipient \
                               LIKE '$local_part'} \
                         {$value} \
                         fail}

If Exim has been built without MySQL support, the following condition
statement should work:

    condition = ${run{/path/to/valid-recipient.pl $local_part@$domain}{1}{0}}

It is possible to use the "domains" statement to restrict the domains
for which the og2list is actually tried (see Exim specification,
chapter 15 for details).

"abuse"(RfC2142), "postmaster"(RfC2821) and other essential local
parts should be handled by a router which precedes the og2list router.
The system_aliases router that is part of Exim's default configuration
file is a good place for this.

=item Transport

Transports are the rules that determine by what means a message should
be delivered that has been assigned to this transport by a router.

  og2list_transport:
    debug_print "T: og2list_transport from \
      $return_path for $local_part@$domain"
    driver = pipe
    freeze_exec_fail
    log_output
    command = OG2LIST_HOME/mail-in.pl \
      -f $return_path -t $local_part@$domain
    current_directory = OG2LIST_HOME
    user = OG2LIST_UID
    group = OG2LIST_GID

=back

=head1 Integration into MTA setup (Postfix 2.x)

Integrating the mail-in.pl into Postfix is best done via a 'transport'.
You will need to dedicate a complete domain or subdomain for that purpose.
Think of it as a special purpose domain like lists.workaround.org where
all accounts have a special meaning. The domain might well be a subdomain
of another domain.

=item main.cf

The '/etc/postfix/main.cf' is Postfix' main configuration file. First you
need to define the domain (as mentioned above) here as a relay domain:

  relay_domains = drupal.workaround.org

If you already have relay domains configured here then just add the domain.

=item transport

As written initially the mail-in.pl will be used as a 'transport'. Transports
are defined in '/etc/postfix/transport'. Just add this line forwarding the
domain to the 'drupal:' service:

  drupal.workaround.org drupal:

It's strongly recommended that you also add these two lines:

  abuse@drupal.workaround.org local:
  postmaster@drupal.workaround.org local:

These forward all mails to the given email addresses to the aliases
'abuse' and 'postmaster' that you probably already have set up in your
'/etc/aliases' file. Every domain needs to have a person reading those
two accounts to deal with technical mail problems or spamming attacks.

Then usually you "hash" these files. Try 'postconf transport_maps' and see
if the result looks like 'hash:/etc/postfix/transport'. After changing
the '/etc/postfix/transport' file run 'postmap /etc/postfix/transport'
and the file '/etc/postfix/transport.db' will be updated.

=item master.cf

Now that you told Postfix to send all mails for your special domain to
the 'drupal:' transport you finally need to define this transport.
Add the following lines to your '/etc/postfix/master.cf' file:

  drupal  unix  -       n       n       -       -       pipe
    flags=DRhu user=www-data argv=/path/modules/og/og2list/mail-in.pl \
		-f ${sender} -t ${recipient}

(Note: the first line needs to start at column 0 in your configuration file.
Indent the next line by whitespace.)

=item Restart Postfix

Finally restart the Postfix service ('postfix reload') and try a
'postfix check' to see whether you made any major mistakes. No output
means that everything is well.

=item Try it

After you created a group in drupal you should be able to send mail to the
alias you defined. Sending a mail to testgroup@drupal.workaround.org should
create a log entry like this in your /var/log/mail.log (or wherever your
syslog daemon writes the mail logs to):

Apr 21 22:12:51 torf postfix/pipe[17969]: 99462FFF2:
  to=<testgroup@drupal.workaround.org>, relay=drupal, delay=1,
	status=sent (drupal.workaround.org)

=head1 Unfinished work, TODO's

=item Distinguish between temporary and fatal error return codes

=item ...

=head1 License

mail-in.pl, mail handler that accepts one mail from STDIN and queues
it to be processed by the og2list Drupal module.

Copyright (C) 2006 Hilko Bengen

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

=cut

use strict;

use FindBin;
use Mail::Verp;
use Email::Address;
use MIME::Parser;
use MIME::Words qw(decode_mimewords);
use DBI;
use Getopt::Std;
use Encode qw(decode);

use File::Basename 'basename';

use vars qw(
	    $dsn $dbh

	    $parser $entity

	    $add_content_stmt $add_content_handle
	    $add_group_stmt $add_group_handle
	    $add_threadinfo_stmt $add_threadinfo_handle
	    $add_dsn_stmt $add_dsn_handle

	    $add_attachment_stmt $add_attachment_handle

	    $dbtype $dbuser $dbauth $dbhost $dbase
	    $mail_domain
	    $sendmail_binary @sendmail_args

	    $envelope_from $envelope_to
	    $from_name $from_address
	    $to_name $to_address

	    $head
	    $subject $msgid $content_type $body

	    %opts
	    @references
	    %attachments
	   );

sub simplify_body($);

# ----------------------------------------------------------------------

# Default values
$mail_domain = "localhost";

Mail::Verp->separator('+');

$dbtype = "mysql";
$dbase  = "";
$dbhost = "localhost";
$dbuser = "";
$dbauth = "";

do "$FindBin::Bin/mail.conf";

# ----------------------------------------------------------------------

$add_content_stmt = <<EOF;
INSERT INTO og2list_incoming_content
(from_address,from_name,subject,msgid,content_type,body)
VALUES (?,?,?,?,?,?);
EOF

$add_group_stmt = <<EOF;
INSERT INTO og2list_incoming_groups
SET mid=(SELECT mid FROM og2list_incoming_content
         WHERE msgid=?), oid=(SELECT nid FROM og2list_groups
                              WHERE recipient=SUBSTRING_INDEX(?, '\@', 1));
EOF

$add_threadinfo_stmt = <<EOF;
INSERT INTO og2list_references
(msgid, reference)
VALUES (?,?);
EOF

$add_dsn_stmt = <<EOF;
UPDATE og2list_outgoing_recipients
SET status = 5
WHERE (mid = ?) AND (recipient_address = ?)
EOF

$add_attachment_stmt = <<EOF;
INSERT INTO og2list_attachment
(mid, filename, content_type, payload, disposition)
VALUES ((SELECT mid FROM og2list_incoming_content
         WHERE msgid=?), ?, ?, ?, ?);
EOF

# ----------------------------------------------------------------------

getopts('dt:f:', \%opts);

$envelope_from = ($opts{'f'} or '');
$envelope_to = ($opts{'t'} or '');

# --------------------
# Try to parse message

$parser = new MIME::Parser;
$parser->output_to_core(1);
$entity = $parser->parse(\*STDIN) or die;
$head = $entity->head;
$head->unfold;

# Pick the first name and address of the first From and To headers, respectively
($from_name, $from_address) = do {
  my @address =
    Email::Address->parse($head->get("From",0));
  ($address[0]->name, $address[0]->address);
};
($to_name, $to_address) = do {
  my @address =
    Email::Address->parse($head->get("To",0));
  ($address[0]->name, $address[0]->address);
};

# Decode subject according to RfC1522/2045, using MIME::Words
foreach (decode_mimewords($head->get("Subject",0))) {
  $subject.=($$_[1]?decode($$_[1], $$_[0]) : $$_[0]);
}

# We make up our own Message-ID header if we don't find one.
$msgid = ( $head->get("Message-ID",0) or
	  ('<'. time() .'.'. int(rand(1000000)) .'@'. $mail_domain .'>'));

chomp $subject; utf8::upgrade($subject);
chomp $msgid;

# -------------------------------------------
# Simplify structure returned by MIME::Parser

($content_type,$body)=simplify_body($entity);

unless (defined $content_type && $content_type =~ /^text\/(plain|html)/) {
  $content_type = 'text/plain';
  $body = "Failed to convert the message $msgid to a meaningful format";
}

@references = split(/\s+/, ( $head->get('References') or
			     $head->get('In-Reply-To') or
			     '' ));

# Setup connection to database if not in debugging mode
unless ($opts{'d'}) {
  $dsn = "DBI:$dbtype:$dbase:$dbhost";
  $dbh = DBI->connect($dsn, $dbuser, $dbauth) or die;
  
  # Work around charset encoding confusion.
  if ($dbtype eq 'mysql') { $dbh->do('SET NAMES utf8'); }
}

if ($envelope_from eq '') {
  # VERP handling
  if ($opts{'d'}) {
    print "Handling bounce for (possible) member ".
      Mail::Verp->decode($envelope_to)."\n";
  } else {
    # Set up queries
    $add_dsn_handle = $dbh->prepare($add_dsn_stmt) or die;

    (my $failed_rcpt,undef) = Mail::Verp->decode($envelope_to);

    $add_dsn_handle->execute($msgid, $failed_rcpt);
  }
  exit 0;
} else {
  if ($opts{'d'}) {
    binmode(STDOUT, ':utf8');
    print <<EOF;
X-Envelope-From: $envelope_from
X-Envelope-To: $envelope_to
From: \"$from_name\" <$from_address>
To: \"$to_name\" <$to_address>
Subject: $subject
Message-Id: $msgid
Content-Type: $content_type
EOF
    print "References: ".join(', ', @references)."\n";
    print "\n$body";
  } else {
    # Set up queries
    $add_content_handle = $dbh->prepare($add_content_stmt) or die;
    $add_threadinfo_handle = $dbh->prepare($add_threadinfo_stmt) or die;
    $add_group_handle = $dbh->prepare($add_group_stmt) or die;

    $add_attachment_handle = $dbh->prepare($add_attachment_stmt) or die;

    $add_content_handle->execute($from_address,
				 $from_name,
				 $subject,
				 $msgid,
				 $content_type,
				 $body);
    $add_group_handle->execute($msgid, $envelope_to);

    foreach (keys %attachments) {
       my %attch = %{$attachments{$_}};

       $add_attachment_handle->execute($msgid,
                                       $attch{'name'},
                                       $attch{'type'},
                                       $attch{'content'},
                                       $attch{'disp'});
    }

    foreach (@references) {
      $add_threadinfo_handle->execute($msgid, $_);
    }
  }
}

# ----------------------------------------------------------------------
sub save_attachment(@) {
  my $entity = shift;
  my $name = $entity->head()->recommended_filename;
  my $type = $entity->effective_type;
  my $disp = $entity->head()->get('Content-Disposition');

  my $filename = basename $name;

  my $maxLen = 16777216; # size of MEDIUMBLOB

  ${$attachments{$filename}}{'name'} = $filename;
  ${$attachments{$filename}}{'type'} = $type;
  ${$attachments{$filename}}{'disp'} = $disp;

  if(my $bh = $entity->bodyhandle) {
    my $io = $bh->open("r");

    ${$attachments{$filename}}{'length'} =
        $io->read(${$attachments{$filename}}{'content'}, $maxLen);

    $io->close;
  }

  if ($opts{'d'}) {
    my $attachment = File::Spec->catfile('/tmp', $filename);

    unless( -f $attachment ) {
        open( FH, ">$attachment");
        print FH ${$attachments{$filename}}{'content'};
        close(FH);
    }
  }
}

# ----------------------------------------------------------------------

sub simplify_body($) {
  my $entity = shift;
  my $body;

  foreach ($entity->mime_type) {
    chomp;

    # Simple text/plain or tex/html messge
    m/^text\/(plain|html)$/ && do {
      my $charset = ($entity->head()->mime_attr('content-type.charset') or
		     'US-ASCII');
      $body = decode($charset, $entity->bodyhandle()->as_string);
      utf8::upgrade($body);
      return $entity->mime_type(), $body;
    };

    # Typical case: alternative encodings
    m/^multipart\/alternative$/ && do {
      my $type;
      my %alternative;
      # Collect only the first of each type.
      foreach my $part ($entity->parts) {
	($type, $body) = simplify_body($part);
	if ($type =~ /^text/) {
	  next if (exists $alternative{$type});
	  $alternative{$type}=$body;
        }
      }
      if ($alternative{'text/html'}) {
	return 'text/html', $alternative{'text/html'};
      } elsif ($alternative{'text/plain'}) {
	return 'text/plain', $alternative{'text/plain'};
      } else {
	return undef;
      }
    };

    # Mail with attachment(s) (e.g. OE HTML mail with embedded image...)
    # OR
    # S/MIME signed mail
    m/^multipart\/(related|mixed|parallel|signed)$/ && do {
      my $type;
      my %alternative;
      # Concatenate parts of same type
      foreach my $part ($entity->parts) {
	($type, $body) = simplify_body($part);
	if ($type =~ /^text/) {
	  $alternative{$type}.=$body;
	}
        else {
            # For all non-text based components
            # store the attachments for use by
            # mail-out
            save_attachment( $part );
        }
      }
      if ($alternative{'text/html'}) {
	return 'text/html', $alternative{'text/html'};
      } elsif ($alternative{'text/plain'}) {
	return 'text/plain', $alternative{'text/plain'};
      } else {
	return undef;
      }
    };

    # RfC 2046, Section 5.1.5
    m/^multipart\/digest$/ && do {
    };

    # RfC 2046, Section 5.1.7
    m/^multipart\/(.*)$/ && do {
    };
  }
  return undef;
}

# Local Variables:
# mode:cperl
# End:
